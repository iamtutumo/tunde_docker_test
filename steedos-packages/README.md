Huayan Rubik's Cube software package
Huayan Rubik's Cube is designed with the smallest kernel, and the content under this folder expands the functions of Huayan Rubik's Cube in the form of software package plug-ins.

app-* Application package
WebApp - * Front-end extension package
standard-* standard object package
service-* Service extension package

The software package that starts with ee_ is the enterprise version of the software package, which can only be used after purchasing a license.
